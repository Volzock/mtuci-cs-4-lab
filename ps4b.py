# Problem Set 4B
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx

import string


### HELPER CODE ###
def load_words(file_name):
    '''
    file_name (string): the name of the file containing 
    the list of words to load    
    
    Returns: a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    '''
    # print("Loading word list from file...")
    # inFile: file
    inFile = open(file_name, 'r')
    # wordlist: list of strings
    wordlist = []
    for line in inFile:
        wordlist.extend([word.lower() for word in line.split(' ')])
    # print("  ", len(wordlist), "words loaded.")
    return wordlist


def is_word(word_list, word):
    word = word.lower()
    word = word.strip(" !@#$%^&*()-_+={}[]|\:;'<>?,./\"")
    return word in word_list


def get_story_string():
    """
    Returns: a story in encrypted text.
    """
    f = open("story.txt", "r")
    story = str(f.read())
    f.close()
    return story


### END HELPER CODE ###

WORDLIST_FILENAME = 'words.txt'


class Message(object):
    def __init__(self, text):
        self.message_text = text
        self.valid_words = load_words(WORDLIST_FILENAME)

    def get_message_text(self) -> str:
        return self.message_text

    def get_valid_words(self) -> list:
        return self.valid_words.copy()

    def build_shift_dict(self, shift: int) -> dict:
        shift = (shift % 26) if shift > 0 else shift % 26
        shift_nums = []
        for i in range(26):
            if i + shift >= 26:
                shift_nums.append((i + shift) % 26)
            elif i + shift < 0:
                shift_nums.append(26 + i + shift)
            else:
                shift_nums.append(i + shift)

        shift_dict = {chr(ord('a') + i): chr(ord('a') + shift_nums[i]) for i in range(26)}
        for i in range(26):
            shift_dict[chr(ord('A') + i)] = chr(ord('A') + shift_nums[i])

        return shift_dict

    def apply_shift(self, shift: int) -> str:
        shift_dict = self.build_shift_dict(shift)
        return ''.join(shift_dict.get(char, None) if shift_dict.get(char, None) else char for char in self.message_text)


class PlaintextMessage(Message):
    def __init__(self, text: str, shift: int):
        super().__init__(text)
        self.shift = shift
        self.encryption_dict = self.build_shift_dict(self.shift)
        self.message_text_encrypted = self.apply_shift(self.shift)

    def get_shift(self) -> int:
        return self.shift

    def get_encryption_dict(self) -> dict:
        return self.encryption_dict.copy()

    def get_message_text_encrypted(self) -> str:
        return self.message_text_encrypted

    def change_shift(self, shift: int) -> None:
        self.shift = shift
        self.encryption_dict = self.build_shift_dict(self.shift)
        self.message_text_encrypted = self.apply_shift(self.shift)


class CiphertextMessage(Message):
    def __init__(self, text: str):
        super().__init__(text)

    def decrypt_message(self) -> (int, str):
        decrypt_str = None
        flag = None
        for shift in range(26):
            flag = True

            decrypt_str = self.apply_shift(-shift)

            for decrypt_word in decrypt_str.strip().split():
                if not is_word(self.valid_words, decrypt_word):
                    flag = False

            if flag:
                return 26 - shift, decrypt_str


if __name__ == '__main__':
    test_data = [['hello', 'jgnnq', 2],
                 ['nobody', 'mnancx', -1],
                 ['apple hello', 'bqqmf ifmmp', 27],
                 ['ApPle NoboDy', 'RgGcv EfsfUp', 10001],
                 ['ApPle NoboDy', 'JyYun WxkxMh', -10001],
                 ['ApPle!?/ NoboDy!!', 'JyYun!?/ WxkxMh!!', -10001]]

    for text, crypt_text, shift in test_data:
        print()
        print(f"Test data: simple_text = {text}, crypt_text = {crypt_text}, shift_num = {shift}")
        plaintext = PlaintextMessage(text, shift)
        print(f"Encryption test = {crypt_text == plaintext.get_message_text_encrypted()}")

        ciphertext = CiphertextMessage(crypt_text)
        print(f"Decryption test = {(26 - shift % 26, text) == ciphertext.decrypt_message()}")
        print()
