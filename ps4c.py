# Problem Set 4C
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx

import string

import ps4a
from ps4a import get_permutations

### HELPER CODE ###
def load_words(file_name):
    '''
    file_name (string): the name of the file containing 
    the list of words to load    
    
    Returns: a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    '''
    # inFile: file
    inFile = open(file_name, 'r')
    # wordlist: list of strings
    wordlist = []
    for line in inFile:
        wordlist.extend([word.lower() for word in line.split(' ')])
    return wordlist

def is_word(word_list, word):
    '''
    Determines if word is a valid word, ignoring
    capitalization and punctuation

    word_list (list): list of words in the dictionary.
    word (string): a possible word.
    
    Returns: True if word is in word_list, False otherwise

    Example:
    >>> is_word(word_list, 'bat') returns
    True
    >>> is_word(word_list, 'asdf') returns
    False
    '''
    word = word.lower()
    word = word.strip(" !@#$%^&*()-_+={}[]|\:;'<>?,./\"")
    return word in word_list


### END HELPER CODE ###

WORDLIST_FILENAME = 'words.txt'

# you may find these constants helpful
VOWELS_LOWER = 'aeiou'
VOWELS_UPPER = 'AEIOU'
CONSONANTS_LOWER = 'bcdfghjklmnpqrstvwxyz'
CONSONANTS_UPPER = 'BCDFGHJKLMNPQRSTVWXYZ'


class SubMessage(object):
    def __init__(self, text):
        self.message_text = text
        self.valid_words = load_words(WORDLIST_FILENAME)
    
    def get_message_text(self):
        return self.message_text

    def get_valid_words(self):
        return self.valid_words.copy()
                
    def build_transpose_dict(self, vowels_permutation):
        vowels = 'aeiou'
        transpoce_dict = {}

        if set(vowels_permutation) == set(vowels):
            transpoce_dict = {vowels[i]: vowels_permutation[i] for i in range(len(vowels))}

            vowels, vowels_permutation = vowels.upper(), vowels_permutation.upper()
            for i in range(len(vowels)):
                transpoce_dict[vowels[i]] = vowels_permutation[i]

        return transpoce_dict
    
    def apply_transpose(self, transpose_dict):
        return ''.join(transpose_dict[char] if transpose_dict.get(char, None) is not None else char for char in self.message_text)


class EncryptedSubMessage(SubMessage):
    def __init__(self, text):
        super().__init__(text)

    def decrypt_message(self):
        permutations = ps4a.get_permutations('aeiou')
        for permutation in permutations:
            flag = True

            for word in self.apply_transpose(self.build_transpose_dict(permutation)).split():
                if not is_word(self.valid_words, word):
                    flag = False

            if flag:
                return self.apply_transpose(self.build_transpose_dict(permutation))
    

if __name__ == '__main__':
    test_data = [["Hello world!", "eaiuo", "Hallu wurld!"],
                 ["//Nobody?!//", "ouiae", "//Nabady?!//"],
                 ["Apple", "aeiou", "Apple"],
                 ["HEllo apple nObody nicE cUbe", "iouea", "HOlle ipplo nEbedy nucO cAbo"]]

    for decrypted_text, permutation, encrypted_text in test_data:
        dec_message = SubMessage(decrypted_text)
        check_encrypted_text = dec_message.apply_transpose(dec_message.build_transpose_dict(permutation))
        enc_message = EncryptedSubMessage(encrypted_text)
        check_decrypt_text = enc_message.decrypt_message()

        print()
        print(f"Test data: decrypted text = {decrypted_text}, permutation = {permutation}, encrypted text = {encrypted_text}")
        print(f"Encrypted test: {check_encrypted_text == encrypted_text}")
        print(f"Decrypted test: {check_decrypt_text == decrypted_text}")
        print()