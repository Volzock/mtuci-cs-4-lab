# Problem Set 4A
# Name: Nikita Kozlov (volzock)
# Collaborators:
# Time Spent: x:xx

def get_permutations(sequence: str) -> list:
    if len(sequence) == 1 or len(sequence) == 0:
        return [sequence]

    permutations = []
    for permutation in get_permutations(sequence[1:]):
        ls = list(permutation)
        for i in range(len(sequence)):
            tmp_ls = ls.copy()
            tmp_ls.insert(i, sequence[0])
            permutations.append("".join(tmp_ls))

    return permutations


if __name__ == '__main__':
    test_inputs = ['a', 'ab', 'abc', 'abcd']
    test_outputs = [['a'], ['ab', 'ba'], ['abc', 'bac', 'bca', 'acb', 'cab', 'cba'],
      ['abcd', 'bacd', 'bcad', 'bcda', 'acbd', 'cabd', 'cbad', 'cbda', 'acdb', 'cadb', 'cdab', 'cdba', 'abdc',\
      'badc', 'bdac', 'bdca', 'adbc', 'dabc', 'dbac', 'dbca', 'adcb', 'dacb', 'dcab', 'dcba']]

    for i in range(len(test_inputs)):
        if get_permutations(test_inputs[i]) != test_outputs[i]:
            print("Test failed, please rework code")
            exit(1)

    print("Test successed, Nice)")

